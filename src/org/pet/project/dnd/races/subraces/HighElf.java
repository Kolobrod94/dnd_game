package org.pet.project.dnd.races.subraces;

import org.pet.project.dnd.Sex;
import org.pet.project.dnd.races.Elf;

public class HighElf extends Elf {
    private static final String DESCRIPTION = """
            Высший эльф.
            Поскольку вы — высший эльф, у вас острый ум и вы знакомы, по крайней мере, с основами магии.
            Во многих фэнтезийных игровых мирах существует два вида высших эльфов. Один вид высокомерен и замкнут,
            считая себя выше не-эльфов и даже других эльфов. Другой вид более распространён и дружелюбен, и часто
            встречается среди людей и других рас.
                        
            Увеличение характеристик.
            Значение вашего Интеллекта увеличивается на 1.
                        
            Владение эльфийским оружием.
            Вы владеете длинным мечом, коротким мечом, коротким луком и длинным луком.
                        
            Заклинание.
            Вы знаете один заговор из списка заклинаний волшебника.
            Базовой заклинательной характеристикой для него является Интеллект.
                        
            Дополнительный язык.
            Вы можете говорить,читать и писать на ещё одном языке, на ваш выбор.
            """;

    public HighElf(String name,
                   String characterClass,
                   String background,
                   String worldview,
                   Integer age,
                   Integer height,
                   Integer weight,
                   Sex sex) {
        super(name,
                characterClass,
                background,
                worldview,
                age,
                height,
                weight,
                sex,
                DESCRIPTION);
        setIntelligence(getIntelligence() + 1);
        setAgility(10);
    }
}
