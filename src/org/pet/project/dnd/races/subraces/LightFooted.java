package org.pet.project.dnd.races.subraces;

import org.pet.project.dnd.Sex;
import org.pet.project.dnd.races.Halfling;

public class LightFooted extends Halfling {
    private static final String DESCRIPTION = """
                    Легконогий
                    Легконогие полурослики умеют отлично скрываться, в том числе используя других существ как укрытие. 
                    Вы склонны быть приветливым и хорошо ладить с другими.
                        
                    Легконогие более других склонны к перемене мест, и часто селятся по соседству с другими народами,
                    или ведут кочевую жизнь.
                        
                    Увеличение характеристик.
                    Значение вашей Харизмы увеличивается на 1.
                        
                    Естественная скрытность.
                    Вы можете предпринять попытку скрыться даже если заслонены только существом,
                    превосходящими вас в размере как минимум на одну категорию.
            """;


    public LightFooted(String name,
                       String characterClass,
                       String background,
                       String worldview,
                       Integer age,
                       Integer height,
                       Integer weight,
                       Sex sex) {
        super(name,
                characterClass,
                background,
                worldview,
                age,
                height,
                weight,
                sex,
                DESCRIPTION);
        setCharisma(getCharisma() + 1);
        setAgility(10);
    }
}
