package org.pet.project.dnd.races.subraces;

import org.pet.project.dnd.Sex;
import org.pet.project.dnd.races.Dwarve;

public class HillDwarf extends Dwarve {

    private static final String DESCRIPTION = """
            Холмовой дварф
            Будучи холмовым дварфом вы обладаете обострёнными чувствами, развитой интуицией и замечательной стойкостью.
                            
            Увеличение характеристик.
            Значение вашей Мудрости увеличивается на 1.
                            
            Дварфская выдержка.
            Максимальное значение ваших хитов увеличивается на 1, и вы получаете 1дополнительный хит с каждым новым уровнем.
            """;

    public HillDwarf(String name,
                     String characterClass,
                     String background,
                     String worldview,
                     Integer age,
                     Integer height,
                     Integer weight,
                     Sex sex) {
        super(name,
                characterClass,
                background,
                worldview,
                age,
                height,
                weight,
                sex,
                DESCRIPTION);
        setWisdom(getWisdom() + 1);
        setSurvivability(10);
    }
}
