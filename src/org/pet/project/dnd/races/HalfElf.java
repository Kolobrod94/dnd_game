package org.pet.project.dnd.races;

import org.pet.project.dnd.*;
import org.pet.project.dnd.Character;

public class HalfElf extends Character {
    private static final String DESCRIPTION = """
            Особенности полуэльфов
            Ваш персонаж-полуэльф обладает некоторыми качествами, обычными для эльфов, и некоторыми, присущими только полуэльфам.
                        
            Увеличение характеристик.
            Значение вашей Харизмы увеличивается на 2, а значения двух других характеристик на ваш выбор увеличиваются на 1.
                        
            Возраст.
            Полуэльфы взрослеют с той же скоростью, что и люди, и достигают зрелости к 20 годам. Они живут гораздо дольше людей, часто пересекая рубеж в 180 лет.
                        
            Мировоззрение.
            Полуэльфы унаследовали склонность к хаосу от своих эльфийских предков.
            Они одинаково ценят и личную свободу и творческое самовыражение, не проявляя ни тяги к лидерству,
            ни желания следовать за лидером. Их раздражают правила и чужие требования, и иногда они оказываются
            ненадёжными или непредсказуемыми.
                        
            Размер.
            Полуэльфы почти такого же размера,как и люди. Их рост колеблется от 5 до 6 футов (от155 до 183 сантиметров).
            Ваш размер — Средний.
                        
            Скорость.
            Ваша базовая скорость ходьбы - 30 футов.
                        
            Темное зрение.
            Благодаря вашей эльфийской крови, вы обладаете превосходным зрением в темноте и при тусклом освещении.
            На расстоянии в 60 футов вы при тусклом освещении можете видеть так, как будто это яркое освещение,
            и в темноте так, как будто это тусклое освещение. В темноте вы не можете различать цвета, только оттенки серого.
                        
            Наследие фей.
            Вы совершаете с преимуществом спасброски от очарования, и вас невозможно магически усыпить.
                        
            Гибкость навыков.
            Вы получаете владение двумя навыками на ваш выбор.
                        
            Языки.
            Вы можете говорить, читать и писать на Общем, Эльфийском, и ещё одном языке на ваш выбор.
            """;

    public HalfElf(String name,
                   String characterClass,
                   String background,
                   String worldview,
                   Integer age,
                   Integer height,
                   Integer weight,
                   Sex sex,
                   SpecialCharacteristic type1,
                   SpecialCharacteristic type2,
                   String description) {
        super(name,
                characterClass,
                background,
                worldview,
                age,
                height,
                weight,
                30,
                CharacterSize.MEDIUM,
                sex,
                description != null ? DESCRIPTION + description : DESCRIPTION);
        setCharisma(getCharisma() + 2);

        if ((type1 != null || type2 != null) & (!type1.equals(type2)) &
                (!SpecialCharacteristic.CHARISMA.equals(type1) || !SpecialCharacteristic.CHARISMA.equals(type2))) {
            switch (type1) {
                case POWER -> setPower(getPower() + 1);
                case AGILITY -> setAgility(getAgility() + 1);
                case SURVIVABILITY -> setSurvivability(getSurvivability() + 1);
                case INTELLIGENCE -> setIntelligence(getIntelligence() + 1);
                case WISDOM -> setWisdom(getWisdom() + 1);
            }
            switch (type2) {
                case POWER -> setPower(getPower() + 1);
                case AGILITY -> setAgility(getAgility() + 1);
                case SURVIVABILITY -> setSurvivability(getSurvivability() + 1);
                case INTELLIGENCE -> setIntelligence(getIntelligence() + 1);
                case WISDOM -> setWisdom(getWisdom() + 1);
            }
        }
        else {
            throw new DnDException("Ошибка! Параметры неверны.");
        }


        // Выберите две любые характеристики +1
    }

    public HalfElf(String name,
                   String characterClass,
                   String background,
                   String worldview,
                   Integer age,
                   Integer height,
                   Integer weight,
                   Sex sex,
                   SpecialCharacteristic type1,
                   SpecialCharacteristic type2) {
        this(name,
                characterClass,
                background,
                worldview,
                age,
                height,
                weight,
                sex,
                type1,
                type2,
                null);
    }
}
