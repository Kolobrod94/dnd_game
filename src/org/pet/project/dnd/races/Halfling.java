package org.pet.project.dnd.races;

import org.pet.project.dnd.Character;
import org.pet.project.dnd.CharacterSize;
import org.pet.project.dnd.Sex;

public class Halfling extends Character {
    private static final String DESCRIPTION = """
            Особенности полуросликов
            Как и другие полурослики, ваш персонаж обладает определёнными качествами.
                        
            Увеличение характеристик.
            Значение вашей Ловкости увеличивается на 2.
                        
            Возраст.
            Полурослики достигают зрелости к 20 годам, и обычно живут до середины своего второго столетия.
                        
            Мировоззрение.
            Большинство полуросликов законно-добрые. Как правило, они добросердечны илюбезны, не выносят чужой боли и
            не терпят притеснения. Также они являются поборниками порядка и традиций, сильно полагаясь на обществои
            предпочитая проверенные пути.
                        
            Размер.
            Полурослики в среднем примерно 3фута ростом и весят около 40 фунтов. Ваш размер — Маленький.
                        
            Скорость.
            Ваша базовая скорость перемещения — 25 футов.
                        
            Удачливый.
            Если при броске атаки, проверке характеристики или спасброске у вас выпало «1», вы можете перебросить кость,
            и должны использовать новый результат.
                        
            Храбрый.
            У вас есть преимущество в спасательных бросках против страха.
                        
            Проворство полуросликов.
            Вы можете проходить сквозь пространство, занятое существами,чей размер больше вашего.
                        
            Языки.
            Вы можете говорить, читать и писать на Общем и языке полуросликов.
            Их язык не является секретным, но они не торопятся делиться им с остальными.
            Пишут они мало, и почти не создали собственной литературы, но устные предания у них очень распространены.
            Почти все полурослики знают Общий, чтобы общаться с людьми в землях, где они обитают, или по которым странствуют
            """;

    public Halfling(String name,
                    String characterClass,
                    String background,
                    String worldview,
                    Integer age,
                    Integer height,
                    Integer weight,
                    Sex sex,
                    String description) {
        super(name,
                characterClass,
                background,
                worldview,
                age,
                height,
                weight,
                25,
                CharacterSize.LITTLE,
                sex,
                description != null ? DESCRIPTION + description : DESCRIPTION);
        setAgility(getAgility() + 2);
    }

    public Halfling(String name,
                    String characterClass,
                    String background,
                    String worldview,
                    Integer age,
                    Integer height,
                    Integer weight,
                    Sex sex) {
        this(name,
                characterClass,
                background,
                worldview,
                age,
                height,
                weight,
                sex,
                null);
    }

}
