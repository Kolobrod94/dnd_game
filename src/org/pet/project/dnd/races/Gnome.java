package org.pet.project.dnd.races;

import org.pet.project.dnd.Character;
import org.pet.project.dnd.CharacterSize;
import org.pet.project.dnd.Sex;

public class Gnome extends Character {
    private static final String DESCRIPTION = """
            Особенности гномов
            Ваш персонаж-гном обладает следующими особенностями, общими для всех гномов.
                        
            Увеличение характеристик.
            Значение вашего Интеллекта увеличивается на 2.
                        
            Возраст.
            Гномы взрослеют с той же скоростью,что и люди, и вероятнее всего к 40 годам переходят к спокойной взрослой жизни.
            Они способныпрожить от 350 до почти 500 лет.
                        
            Мировоззрение.
            Гномы чаще всего добры. Стремящиеся к порядку обычно становятся мудрецами, инженерами, исследователями,
            учёными или изобретателями. Те, кто больше склонны к хаосу, становятся менестрелями, мошенниками,
            путешественниками или искусными ювелирами. Гномы добросердечны, и даже мошенники из них получаются скорее шутливые,
            чем злобные.
                        
            Размер.
            Рост гномов между 3 и 4 футами (91 и122 сантиметрами), а средний вес составляет 40фунтов (18 килограмм).
            Ваш размер — Маленький.
                        
            Скорость.
            Ваша базовая скорость перемещения — 25 футов.
                        
            Темное зрение.
            Привыкнув к жизни под землёй, вы обладаете превосходным зрением в темноте и при тусклом освещении.
            На расстоянии в 60 футов вы при тусклом освещении можете видеть так, как будто это яркое освещение,
            и в темноте так, как будто это тусклое освещение. В темноте вы не можете различать цвета, только оттенки серого.
                        
            Гномья Хитрость.
            Вы совершаете с преимуществом спасброски Интеллекта, Мудрости и Харизмы против магии.
                        
            Языки.
            Вы можете говорить, читать и писатьна Общем и Гномьем языках.
            Гномий язык, использующий дварфский алфавит, хорошо известен благодаря техническим трактатам и каталогам
            знаний об окружающем мире.
            """;

    public Gnome(String name,
                 String characterClass,
                 String background,
                 String worldview,
                 Integer age,
                 Integer height,
                 Integer weight,
                 Sex sex,
                 String description) {
        super(name,
                characterClass,
                background,
                worldview,
                age,
                height,
                weight,
                25,
                CharacterSize.LITTLE,
                sex,
                description != null ? DESCRIPTION + description : DESCRIPTION);

        setIntelligence(getIntelligence() + 2);
    }

    public Gnome(String name,
                 String characterClass,
                 String background,
                 String worldview,
                 Integer age,
                 Integer height,
                 Integer weight,
                 Sex sex) {
        this(name,
                characterClass,
                background,
                worldview,
                age,
                height,
                weight,
                sex,
                null);


    }
}
