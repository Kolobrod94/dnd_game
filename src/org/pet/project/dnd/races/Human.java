package org.pet.project.dnd.races;

import org.pet.project.dnd.Character;
import org.pet.project.dnd.CharacterSize;
import org.pet.project.dnd.Sex;

public class Human extends Character {
    private static final String DESCRIPTION = """
            Особенности людей
            Сложно делать какие-либо обобщения относительно людей, 
            но ваш персонаж-человек обладает следующими особенностями.
                        
            Увеличение характеристик.
            Значение всех ваших характеристик увеличивается на 1.
                        
            Возраст.
            Люди становятся взрослыми в районе 20 лет, и живут менее века.
                        
            Мировоззрение.
            Люди не имеют склонности к определённому мировоззрению.
            Среди них встречаются лучшие и худшие.
                        
            Размер.
            Люди сильно различаются по размерам.
            Некоторые с трудом достигают 5 футов ростом, тогда как другие имеют рост, превосходящий 6 футов.
            Вне зависимости от роста, ваш размер — Средний.
                        
            Скорость.
            Ваша базовая скорость ходьбы - 30 футов.
                        
            Языки.
            Вы можете говорить, читать и писать на Общем и ещё одном языке на ваш выбор.                        
            Люди обычно изучают языки народов, с которыми имеют дело, включая редкие диалекты.
            Они любят разбавлять собственную речь словами, позаимствованными из других языков:
            орочьими ругательствами, эльфийскими музыкальными терминами, дварфийскими военными командами.            
            """;

    public Human(String name,
                 String characterClass,
                 String background,
                 String worldview,
                 Integer age,
                 Integer height,
                 Integer weight,
                 Sex sex,
                 String description) {
        super(name,
                characterClass,
                background,
                worldview,
                age,
                height,
                weight,
                30,
                CharacterSize.MEDIUM,
                sex,
                description != null ? DESCRIPTION + description : DESCRIPTION);
        setPower(getPower() + 1);
        setAgility(getAgility() + 1);
        setSurvivability(getSurvivability() + 1);
        setIntelligence(getIntelligence() + 1);
        setWisdom(getWisdom() + 1);
        setCharisma(getCharisma() + 1);
    }

    public Human(String name,
                 String characterClass,
                 String background,
                 String worldview,
                 Integer age,
                 Integer height,
                 Integer weight,
                 Sex sex) {
        this(name,
                characterClass,
                background,
                worldview,
                age,
                height,
                weight,
                sex,
                null);
    }

}
