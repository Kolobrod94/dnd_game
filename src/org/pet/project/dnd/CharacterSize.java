package org.pet.project.dnd;

public enum CharacterSize {
    LITTLE,
    MEDIUM,
    BIG,
    GIGANTIC;
}
