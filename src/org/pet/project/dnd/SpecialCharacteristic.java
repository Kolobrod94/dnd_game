package org.pet.project.dnd;

public enum SpecialCharacteristic {
    POWER,
    AGILITY,
    SURVIVABILITY,
    INTELLIGENCE,
    WISDOM,
    CHARISMA;
}
