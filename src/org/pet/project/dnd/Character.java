package org.pet.project.dnd;

import java.util.Objects;

public abstract class Character {

    private String description;
    private Integer power;
    private Integer agility;
    private Integer survivability;
    private Integer intelligence;
    private Integer wisdom;
    private Integer charisma;
    private String name;
    private String characterClass;
    private Integer level;
    private String background;
    private String worldview;
    private Integer experience;
    private Integer age;
    private Integer height;
    private Integer weight;
    private Integer speed;
    private CharacterSize characterSize;

    private Sex sex;

    public Character(String name,
                     String characterClass,
                     String background,
                     String worldview,
                     Integer age,
                     Integer height,
                     Integer weight,
                     Integer speed,
                     CharacterSize characterSize,
                     Sex sex,
                     String description) {
        this(null,
                null,
                null,
                null,
                null,
                null,
                name,
                characterClass,
                null,
                background,
                worldview,
                null,
                age,
                height,
                weight,
                speed,
                characterSize,
                sex,
                description);
    }

    public Character(Integer power,
                     Integer agility,
                     Integer survivability,
                     Integer intelligence,
                     Integer wisdom,
                     Integer charisma,
                     String name,
                     String characterClass,
                     Integer level,
                     String background,
                     String worldview,
                     Integer experience,
                     Integer age,
                     Integer height,
                     Integer weight,
                     Integer speed,
                     CharacterSize characterSize,
                     Sex sex,
                     String description) {
        this.power = power;
        if (this.power == null) {
            this.power = 10;
        }
        this.agility = agility;
        if (this.agility == null) {
            this.agility = 10;
        }
        this.survivability = survivability;
        if (this.survivability == null) {
            this.survivability = 10;
        }
        this.intelligence = intelligence;
        if (this.intelligence == null) {
            this.intelligence = 10;
        }
        this.wisdom = wisdom;
        if (this.wisdom == null) {
            this.wisdom = 10;
        }
        this.charisma = charisma;
        if (this.charisma == null) {
            this.charisma = 10;
        }
        this.name = name;
        Objects.requireNonNull(this.name, "Укажите имя героя.");
        this.characterClass = characterClass;
        Objects.requireNonNull(this.characterClass, "Выберите класс.");
        this.level = level;
        if (this.level == null) {
            this.level = 1;
        }
        this.background = background;
        Objects.requireNonNull(this.background, "Выберите предысторию.");
        this.worldview = worldview;
        Objects.requireNonNull(this.worldview, "Выберите мировозрение.");
        this.experience = experience;
        if (this.experience == null) {
            this.experience = 0;
        }
        this.age = age;
        if (this.age <= 0) {
            throw new RuntimeException("Возраст должен быть больше 0.");
        }
        Objects.requireNonNull(this.age, "Укажите возраст.");
        this.height = height;
        Objects.requireNonNull(this.height, "Укажите рост.");
        this.weight = weight;
        Objects.requireNonNull(this.weight, "Укажите вес.");
        this.speed = speed;
        Objects.requireNonNull(this.speed, "Скорость не указана.");
        this.characterSize = characterSize;
        Objects.requireNonNull(this.characterSize, "Размер не указан.");
        this.sex = sex;
        Objects.requireNonNull(this.sex, "Пол указан не верно.");
        this.description = description;
    }

    public Integer getPower() {
        return power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public Integer getAgility() {
        return agility;
    }

    public void setAgility(Integer agility) {
        this.agility = agility;
    }

    public Integer getSurvivability() {
        return survivability;
    }

    public void setSurvivability(Integer survivability) {
        this.survivability = survivability;
    }

    public Integer getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(Integer intelligence) {
        this.intelligence = intelligence;
    }

    public Integer getWisdom() {
        return wisdom;
    }

    public void setWisdom(Integer wisdom) {
        this.wisdom = wisdom;
    }

    public Integer getCharisma() {
        return charisma;
    }

    public void setCharisma(Integer charisma) {
        this.charisma = charisma;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCharacterClass() {
        return characterClass;
    }

    public void setCharacterClass(String characterClass) {
        this.characterClass = characterClass;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getWorldview() {
        return worldview;
    }

    public void setWorldview(String worldview) {
        this.worldview = worldview;
    }

    public Integer getExperience() {
        return experience;
    }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getSpeed() {
        return speed;
    }

    public void setSpeed(Integer speed) {
        this.speed = speed;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
       return "Character{" +
                "description='" + description + '\'' +
                ", power=" + power +
                ", agility=" + agility +
                ", survivability=" + survivability +
                ", intelligence=" + intelligence +
                ", wisdom=" + wisdom +
                ", charisma=" + charisma +
                ", name='" + name + '\'' +
                ", characterClass='" + characterClass + '\'' +
                ", level=" + level +
                ", background='" + background + '\'' +
                ", worldview='" + worldview + '\'' +
                ", experience=" + experience +
                ", age=" + age +
                ", height=" + height +
                ", weight=" + weight +
                ", speed=" + speed +
                ", size='" + characterSize +
                ", sex='" + sex +'\'' +
               '}';
    }

}
